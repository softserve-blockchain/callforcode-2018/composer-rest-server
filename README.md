# composer-rest-server for IBM Cloud

This composer-rest-server should work with the composer runtime and blockchain network deployed by IBM Cloud by IBM Blockchain Starter Plan.

Docker image definition for the composer-rest-server is compatible with the current version of composer runtime on IBM Cloud (0.20.0). It is based on the https://hub.docker.com/r/ibmblockchain/composer-rest-server/ with the version of the composer-rest-server updated to 0.20.0

To build and push it to the dockerhub run `$ ./build-push-image.sh`

The resulting image is pushed to https://hub.docker.com/r/ashapochka/composer-rest-server

## Deploy to IBM Cloud

1. Install cf cli tools (IBM Cloud Foundry toolset) `$ brew install cloudfoundry/tap/cf-cli`
2. Install composer wallet cloudant (to import fabric card to cloudant) `$ npm install -g git+ssh://git@github.com:IBM-Blockchain-Starter-Kit/composer-wallet-cloudant.git`
3. Create a database in the cloudant (provision cloudant service first if it does not exist) called `composer-wallets`
4. Create the file `cardstore-cloudant.json` storing connectivity data for this database. It must look like (the key-value pairs from apikey to username are retrieved from the cloudant service credentials UI):
```json
{
    "composer": {
        "wallet": {
            "type": "@ampretia/composer-wallet-cloudant",
            "options": {
                "database": "composer-wallets",
                "apikey": "xxxxxx",
                "host": "xxxxxx-bluemix.cloudant.com",
                "password": "xxxxxx",
                "port": 443,
                "url": "https://xxxxxx-bluemix:xxxxxx-bluemix.cloudant.com",
                "username": "xxxxxx-bluemix"
            }
        }
    }
}
```
5. Set the cloudant connection variable `$ export NODE_CONFIG=$(cat cardstore-cloudant.json)` (the following commands must be run in the same console where this variable is set).
6. Assuming the card already exists, remaining from the blockchain network deployment run `$ composer card import -f admin@aid-processing.card` to import the card to the cloudant database.
7. Point cf to the API in the region your org, space, cloudant, and blockchain network are deployed `$ cf api https://api.eu-gb.bluemix.net`
8. Login using IBMid username and password with `$ cf login`
9. Run `$ ./deploy-ibm-cloud.sh`
#!/bin/bash
# -*- coding: utf-8 -*-

# first install cf locally and login to ibm cloud in the same command line
# default quota of 1G is insufficient
# card admin@aid-processing needs to be imported to cloudant first (see README)
cf push aid-chain-rest --docker-image ashapochka/composer-rest-server:0.20.0 \
    -c "composer-rest-server -c admin@aid-processing -n never -w true" \
    -i 1 -m 256M -k 2G --no-start --no-manifest
cf set-env aid-chain-rest NODE_CONFIG "${NODE_CONFIG}"
cf start aid-chain-rest